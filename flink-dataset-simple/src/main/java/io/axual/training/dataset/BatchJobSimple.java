/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.axual.training.dataset;

import org.apache.flink.api.java.DataSet;
import org.apache.flink.api.java.ExecutionEnvironment;
import org.apache.flink.api.java.tuple.Tuple4;
import org.apache.flink.api.java.tuple.Tuple5;
import org.apache.flink.api.java.utils.ParameterTool;

/**
 * Skeleton for a Flink Batch Job.
 *
 * <p>For a tutorial how to write a Flink batch application, check the
 * tutorials and examples on the <a href="http://flink.apache.org/docs/stable/">Flink Website</a>.
 *
 * <p>To package your application into a JAR file for execution,
 * change the main class in the POM.xml file to this class (simply search for 'mainClass')
 * and run 'mvn clean package' on the command line.
 */
public class BatchJobSimple {
	public static final String PAR_FILEPATH_IN = "inputFile";
	public static final String PAR_FILEPATH_OUT = "outputFile";
	public static final String FIELD_DELIMITER = ",";
	public static final String LINE_DELIMITER = "\n";


	protected static Tuple4<String, String, Double, String> map(Tuple5<Long, String, String, Double, String> entry) {
		return new Tuple4<String, String, Double, String>(entry.f1, entry.f2, entry.f3, entry.f4);
	}

	public static void main(String[] args) throws Exception {
		// Get parameters
		ParameterTool parameter = ParameterTool.fromArgs(args);
		final String inputPath = parameter.getRequired(PAR_FILEPATH_IN);
		final String outputPath = parameter.getRequired(PAR_FILEPATH_OUT);

		// set up the batch execution environment
		final ExecutionEnvironment env = ExecutionEnvironment.getExecutionEnvironment();

		// Read from CSV
		DataSet<Tuple5<Long, String, String, Double, String>> csvRecords = env.readCsvFile(inputPath)
				.fieldDelimiter(FIELD_DELIMITER)
				.lineDelimiter(LINE_DELIMITER)
				.ignoreFirstLine()
				.types(Long.class, String.class, String.class, Double.class, String.class);

		// TODO: Handle CSV
		// map to filtered form
		// filter all events with a threshold above 0.00
		// write to output
		// csvRecords.

		// execute program
		env.execute("Simple Batch Example");
	}
}
