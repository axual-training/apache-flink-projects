/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.axual.training.dataset;

import org.apache.flink.api.common.functions.Partitioner;
import org.apache.flink.api.java.DataSet;
import org.apache.flink.api.java.ExecutionEnvironment;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.api.java.tuple.Tuple5;
import org.apache.flink.api.java.tuple.Tuple7;
import org.apache.flink.api.java.tuple.Tuple8;
import org.apache.flink.api.java.utils.ParameterTool;

/**
 * Skeleton for a Flink Batch Job.
 *
 * <p>For a tutorial how to write a Flink batch application, check the
 * tutorials and examples on the <a href="http://flink.apache.org/docs/stable/">Flink Website</a>.
 *
 * <p>To package your application into a JAR file for execution,
 * change the main class in the POM.xml file to this class (simply search for 'mainClass')
 * and run 'mvn clean package' on the command line.
 */
public class BatchJobAdvanced {
	public static final String PAR_FILEPATH_ALERTSETTINGS = "alertsettingsFile";
	public static final String PAR_FILEPATH_TRANSACTIONS = "transactionsFile";
	public static final String PAR_FILEPATH_ALERTS = "alertsFile";
	public static final String FIELD_DELIMITER = ",";
	public static final String LINE_DELIMITER = "\n";

	public static void main(String[] args) throws Exception {
		// Get parameters
		ParameterTool parameter = ParameterTool.fromArgs(args);
		final String alertsettingsPath = parameter.getRequired(PAR_FILEPATH_ALERTSETTINGS);
		final String transactionsPath = parameter.getRequired(PAR_FILEPATH_TRANSACTIONS);
		final String alertsPath = parameter.getRequired(PAR_FILEPATH_ALERTS);

		// set up the batch execution environment
		final ExecutionEnvironment env = ExecutionEnvironment.getExecutionEnvironment();

		final AccountIdPartitioner partitioner = new AccountIdPartitioner();

		// Read from CSV
		DataSet<Tuple5<Long, String, String, Double, String>> csvAlertSettings = env.readCsvFile(alertsettingsPath)
				.fieldDelimiter(FIELD_DELIMITER)
				.lineDelimiter(LINE_DELIMITER)
				.ignoreFirstLine()
				.types(Long.class, String.class, String.class, Double.class, String.class)
				.setParallelism(Math.min(5, env.getParallelism()))
				.partitionCustom(partitioner, 1);


		DataSet<Tuple7<Long, Long, String, String, Double, Double, Double>> csvTransactions = env.readCsvFile(transactionsPath)
				.fieldDelimiter(FIELD_DELIMITER)
				.lineDelimiter(LINE_DELIMITER)
				.ignoreFirstLine()
				.types(Long.class, Long.class, String.class, String.class, Double.class, Double.class, Double.class)
				.setParallelism(Math.min(5, env.getParallelism()))
				.partitionCustom(partitioner, 2);


		// TODO:  Handle CSV
		// join on accountIds
		// filter the result
		// map to the alert
		// write the alerts
//		csvTransactions.;

		// execute program
		env.execute("Advanced Batch Example");

	}

	// filter only if the alertsetting is found, and the balance before is above the threshold and balance after below or equal to the threshold
	private static boolean filter(Tuple2<Tuple7<Long, Long, String, String, Double, Double, Double>, Tuple5<Long, String, String, Double, String>> joined) {
		Tuple7<Long, Long, String, String, Double, Double, Double> transaction = joined.f0;
		Tuple5<Long, String, String, Double, String> alertSetting = joined.f1;
		return transaction.f5 > alertSetting.f3 && transaction.f6 <= alertSetting.f3;
	}

	private static Tuple8<Long, String, String, Double, Double, Double, String, String>
	map(Tuple2<Tuple7<Long, Long, String, String, Double, Double, Double>, Tuple5<Long, String, String, Double, String>> joined) {
		Tuple7<Long, Long, String, String, Double, Double, Double> transaction = joined.f0;
		Tuple5<Long, String, String, Double, String> alertSetting = joined.f1;
		return new Tuple8<Long, String, String, Double, Double, Double, String, String>(
				transaction.f0
				, transaction.f2
				, transaction.f3
				, transaction.f4
				, transaction.f5
				, transaction.f6
				, alertSetting.f2
				, alertSetting.f4
		);
	}

	private static class AccountIdPartitioner implements Partitioner<String> {
		@Override
		public int partition(String value, int nrOfPartitions) {
			return Math.abs(value.hashCode() % nrOfPartitions);
		}
	}
}
