/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.axual.training.datastream;

import org.apache.flink.api.common.serialization.SimpleStringSchema;
import org.apache.flink.api.common.typeinfo.TypeInformation;
import org.apache.flink.api.java.tuple.Tuple4;
import org.apache.flink.api.java.utils.ParameterTool;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.connectors.kafka.FlinkKafkaConsumer;
import org.apache.flink.streaming.connectors.kafka.FlinkKafkaProducer;
import org.apache.flink.streaming.connectors.kafka.KafkaDeserializationSchema;
import org.apache.flink.streaming.connectors.kafka.KafkaSerializationSchema;
import org.apache.flink.util.Collector;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.producer.ProducerRecord;

import java.util.Properties;

import javax.annotation.Nullable;

/**
 * Skeleton for a Flink Streaming Job.
 *
 * <p>For a tutorial how to write a Flink streaming application, check the
 * tutorials and examples on the <a href="http://flink.apache.org/docs/stable/">Flink Website</a>.
 *
 * <p>To package your application into a JAR file for execution, run
 * 'mvn clean package' on the command line.
 *
 * <p>If you change the name of the main class (with the public static void main(String[] args))
 * method, change the respective entry in the POM.xml file (simply search for 'mainClass').
 */
public class StreamingJobSimple {
	// expected value payment-transactionalertsettings
	public static final String PAR_TOPIC_IN = "inputTopic";
	// expected value payment-filteredtransactionalertsettings
	public static final String PAR_TOPIC_OUT = "outputTopic";
	public static final String PAR_BOOTSTRAP_SERVERS = "bootstrapServers";
	public static final String FIELD_DELIMITER = ",";

	public static void main(String[] args) throws Exception {
		// set up the streaming execution environment
		final StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
		// Get parameters
		ParameterTool parameter = ParameterTool.fromArgs(args);
		final String inputTopic = parameter.getRequired(PAR_TOPIC_IN);
		final String outputTopic = parameter.getRequired(PAR_TOPIC_OUT);
		final String bootstrapServers = parameter.getRequired(PAR_BOOTSTRAP_SERVERS);

		Properties properties = new Properties();
		properties.setProperty("bootstrap.servers", bootstrapServers);

		KafkaStringSchema consumeSchema = new KafkaStringSchema(inputTopic);
		FlinkKafkaConsumer<String> consumerAlertSettings = new FlinkKafkaConsumer(inputTopic, consumeSchema, properties);
		consumerAlertSettings.setStartFromEarliest();

		KafkaStringSchema produceSchema = new KafkaStringSchema(outputTopic);
		FlinkKafkaProducer<String> producerFilteredAlertSettings = new FlinkKafkaProducer<String>(outputTopic, produceSchema, properties, FlinkKafkaProducer.Semantic.AT_LEAST_ONCE);
		producerFilteredAlertSettings.setLogFailuresOnly(false);

		DataStream<String> alertSettingsStream = env.addSource(consumerAlertSettings);

		// TODO: Handle CSVs
		// map to tuple
		// filter all alerts with threshold above 0.00
		// flatten to string
		// write to filtered alertsettings topic
		// alertSettingsStream.

		// execute program
		env.execute("Simple Stream Example");
	}

	private static void flatmap(String record, Collector<Tuple4<String, String, Double, String>> collector) {
		String[] fields = record.split(FIELD_DELIMITER, 0);
		if (fields.length < 5) {
			return;
		}
		String accountId = fields[1];
		String name = fields[2];
		Double threshold = Double.parseDouble(fields[3]);
		String customMessage = fields[4];
		collector.collect(new Tuple4<String, String, Double, String>(accountId, name, threshold, customMessage));
	}

	private static String flatten(Tuple4<String, String, Double, String> data) {
		return String.format("%s,%s,%.2f,%s", data.f0, data.f1, data.f2, data.f3);
	}

	private static class KafkaStringSchema implements KafkaSerializationSchema<String>, KafkaDeserializationSchema<String> {
		private final String topic;
		private final SimpleStringSchema innerSchema;

		private KafkaStringSchema(String topic) {
			this.topic = topic;
			this.innerSchema = new SimpleStringSchema();
		}

		@Override
		public boolean isEndOfStream(String nextElement) {
			return innerSchema.isEndOfStream(nextElement);
		}

		@Override
		public String deserialize(ConsumerRecord<byte[], byte[]> consumerRecord) throws Exception {
			return innerSchema.deserialize(consumerRecord.value());
		}

		@Override
		public ProducerRecord<byte[], byte[]> serialize(String value, @Nullable Long timestamp) {
			return new ProducerRecord<>(topic, new byte[0], innerSchema.serialize(value));
		}

		@Override
		public TypeInformation<String> getProducedType() {
			return innerSchema.getProducedType();
		}
	}

}
